﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP.Inventory.Entities
{
    public class Stock : Entity<long>, IHasCreationTime
    {
        public string ProdCode { get; set; }


        public int Count { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
