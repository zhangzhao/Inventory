﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP.Inventory.Entities
{
    public class History : Entity<long>
    {
        public string ProdCode { get; set; }
        public int Changes { get; set; }
        public DateTime InDateTime { get; set; }

        public string InUserId { get; set; }

        public DateTime? ModiDateTime { get; set; }

        public string ModiUserId { get; set; }
    }
}
