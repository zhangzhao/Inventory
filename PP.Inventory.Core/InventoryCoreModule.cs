﻿using System.Reflection;
using Abp.Modules;

namespace PP.Inventory
{
    public class InventoryCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
