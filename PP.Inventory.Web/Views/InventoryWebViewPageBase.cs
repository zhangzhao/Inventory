﻿using Abp.Web.Mvc.Views;

namespace PP.Inventory.Web.Views
{
    public abstract class InventoryWebViewPageBase : InventoryWebViewPageBase<dynamic>
    {

    }

    public abstract class InventoryWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected InventoryWebViewPageBase()
        {
            LocalizationSourceName = InventoryConsts.LocalizationSourceName;
        }
    }
}