﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PP.Inventory.Web.Controllers
{
    public class StockController : Controller
    {
        // GET: Stock
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult StockSearch()
        {
            return View();
        }
        public ActionResult UpdateStock()
        {
            return View();
        }
        public JsonResult GetStock()
        {
            string spage = Request.Params["page"];//查询的页数
            string srows = Request.Params["rows"];//每页的数据数

            string sidx = Request.Params["sidx"];//排序列名
            string sord = Request.Params["sord"];//排序方式

            //GridDataModel model = new GridDataModel();
            //List<SampleDto> list = new List<SampleDto>();
            //list.Add(new SampleDto
            //{
            //    StyleCode = "STYLE001",
            //    StyleName = "一般",
            //    SaleAmt = 10000,
            //    SaleQty = 1200,
            //    UseYN = "Y",
            //    Season = "SP",
            //    SaleDate = Convert.ToDateTime("2014-11-22")
            //});
            //list.Add(new SampleDto
            //{
            //    StyleCode = "STYLE002",
            //    StyleName = "儿童",
            //    SaleAmt = 10000,
            //    SaleQty = 1200,
            //    UseYN = "Y",
            //    Season = "AU",
            //    SaleDate = Convert.ToDateTime("2014-11-24")
            //});
            //model.data = list;
            //model.page = 2; //查询的页数
            //model.records = 100; //全部数据数
            //model.total = 11; //页面数
            return Json(new { data = "model" }, JsonRequestBehavior.AllowGet);

        }
    }
}