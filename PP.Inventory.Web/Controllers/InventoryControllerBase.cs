﻿using Abp.Web.Mvc.Controllers;

namespace PP.Inventory.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class InventoryControllerBase : AbpController
    {
        protected InventoryControllerBase()
        {
            LocalizationSourceName = InventoryConsts.LocalizationSourceName;
        }
    }
}