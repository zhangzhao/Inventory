﻿using System.Web.Mvc;

namespace PP.Inventory.Web.Controllers
{
    public class HomeController : InventoryControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}