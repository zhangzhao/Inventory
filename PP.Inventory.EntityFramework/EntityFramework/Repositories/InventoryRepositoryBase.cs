﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace PP.Inventory.EntityFramework.Repositories
{
    public abstract class InventoryRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<InventoryDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected InventoryRepositoryBase(IDbContextProvider<InventoryDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class InventoryRepositoryBase<TEntity> : InventoryRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected InventoryRepositoryBase(IDbContextProvider<InventoryDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
