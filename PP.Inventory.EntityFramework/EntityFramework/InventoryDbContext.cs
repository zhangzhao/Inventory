﻿using Abp.EntityFramework;
using PP.Inventory.Entities;
using System.Data.Entity;

namespace PP.Inventory.EntityFramework
{
    public class InventoryDbContext : AbpDbContext
    {
        //TODO: Define an IDbSet for each Entity...

        //Example:
        public virtual IDbSet<Stock> Stock { get; set; }
        public virtual IDbSet<History> History { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public InventoryDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in InventoryDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of InventoryDbContext since ABP automatically handles it.
         */
        public InventoryDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }
    }
}
