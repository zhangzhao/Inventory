﻿using Abp.Domain.Repositories;
using PP.Inventory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP.Inventory.Repository
{
    public interface IStockRepository : IRepository<Stock, long>
    {
    }
}
