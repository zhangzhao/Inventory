namespace PP.Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addhistory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Histories",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProdName = c.String(),
                        Changes = c.Int(nullable: false),
                        InDateTime = c.DateTime(nullable: false),
                        InUserId = c.String(),
                        ModiDateTime = c.DateTime(),
                        ModiUserId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Histories");
        }
    }
}
