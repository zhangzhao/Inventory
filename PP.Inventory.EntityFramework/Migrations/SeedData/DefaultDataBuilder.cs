﻿using PP.Inventory.Entities;
using PP.Inventory.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP.Inventory.Migrations.SeedData
{
    public class DefaultDataBuilder
    {
        private readonly InventoryDbContext _context;

        public DefaultDataBuilder(InventoryDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            CreateDefaultData();
        }

        private void CreateDefaultData()
        {

            Stock stock = new Stock { ProdCode = "test", Count = 10, CreationTime = DateTime.Now };
            _context.Stock.Add(stock);
            _context.SaveChanges();
        }
    }
}
