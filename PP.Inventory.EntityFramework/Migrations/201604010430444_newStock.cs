namespace PP.Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newStock : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Histories", "ProdCode", c => c.String());
            AddColumn("dbo.Stocks", "ProdCode", c => c.String());
            DropColumn("dbo.Histories", "ProdName");
            DropColumn("dbo.Stocks", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Stocks", "Name", c => c.String());
            AddColumn("dbo.Histories", "ProdName", c => c.String());
            DropColumn("dbo.Stocks", "ProdCode");
            DropColumn("dbo.Histories", "ProdCode");
        }
    }
}
