using PP.Inventory.Migrations.SeedData;
using System.Data.Entity.Migrations;

namespace PP.Inventory.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Inventory.EntityFramework.InventoryDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Inventory";
        }

        protected override void Seed(Inventory.EntityFramework.InventoryDbContext context)
        {
            // This method will be called every time after migrating to the latest version.
            // You can add any seed data here...
            new DefaultDataBuilder(context).Build();
        }
    }
}
