﻿using System.Data.Entity;
using System.Reflection;
using Abp.EntityFramework;
using Abp.Modules;
using PP.Inventory.EntityFramework;

namespace PP.Inventory
{
    [DependsOn(typeof(AbpEntityFrameworkModule), typeof(InventoryCoreModule))]
    public class InventoryDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Database.SetInitializer<InventoryDbContext>(null);
        }
    }
}
