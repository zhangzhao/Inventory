﻿using Abp.Application.Services;

namespace PP.Inventory
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class InventoryAppServiceBase : ApplicationService
    {
        protected InventoryAppServiceBase()
        {
            LocalizationSourceName = InventoryConsts.LocalizationSourceName;
        }
    }
}