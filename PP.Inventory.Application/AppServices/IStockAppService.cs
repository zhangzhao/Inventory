﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using PP.Inventory.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP.Inventory.AppServices
{
    public interface IStockAppService : IApplicationService
    {
        Task<PagedResultOutput<StockOutput>> GetStock(GetStockInput input);

        StockOutput GetStockByName(string ProdCode);

        UpdateStockOutput UpdateStock(UpdateStockInput input);

        UpdateStockOutput UpdateStockList(List<UpdateStockInput> input);
    }
}
