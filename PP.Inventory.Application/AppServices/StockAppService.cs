﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using PP.Inventory.Dto;
using PP.Inventory.Entities;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace PP.Inventory.AppServices
{
    public class StockAppService : ApplicationService, IStockAppService
    {
        private readonly IRepository<Stock, long> _stockRepository;
        private readonly IRepository<History, long> _historyRepository;

        public StockAppService(IRepository<Stock, long> stockRepository, IRepository<History, long> historyRepository)
        {
            _stockRepository = stockRepository;
            _historyRepository = historyRepository;

        }

        public async Task<PagedResultOutput<StockOutput>> GetStock(GetStockInput input)
        {
            var query = from s in _stockRepository.GetAll()
                        orderby s.CreationTime
                        select s;
            var totalCount = await query.CountAsync();
            var stocks = await query.PageBy(input).ToListAsync();

            return new PagedResultOutput<StockOutput>(totalCount, stocks.Select(stock =>
            {
                var dto = stock.MapTo<StockOutput>();
                return dto;
            }).ToList());
        }

      
        public StockOutput GetStockByName(string ProdCode)
        {
            var query = from s in _stockRepository.GetAll()
                        where s.ProdCode == ProdCode
                        select s;
            var dto = query.FirstOrDefault().MapTo<StockOutput>();
            return dto;
        }

         [UnitOfWork]
        public UpdateStockOutput UpdateStock(UpdateStockInput input)
        {
            UpdateStockOutput output = new UpdateStockOutput();
            var query = from s in _stockRepository.GetAll()
                        where s.ProdCode == input.ProdCode
                        select s;
            var dto = query.FirstOrDefault();
          
            if (dto != null && dto.ProdCode == input.ProdCode)
            {
                dto.Count += input.Count;
                dto.CreationTime = DateTime.Now;
                output.Id = dto.Id;
                output.ProdCode = dto.ProdCode;
                output.PreCount = dto.Count;
            }
            else
            {
                dto = new Stock() { ProdCode = input.ProdCode, CreationTime = DateTime.Now, Count = input.Count };
               
                output.ProdCode = dto.ProdCode;
                output.PreCount = 0;
            }
            //_stockRepository.InsertOrUpdateAsync(dto);

            if (dto.Count < 0) {
                output.UpdateStatus = "Message:Stock not enough";
                return output;
            }
            _stockRepository.InsertOrUpdateAsync(dto);
            var historyDto = new History {
                ProdCode = input.ProdCode,
                Changes = input.Count,
                InDateTime = DateTime.Now,
                InUserId = input.InUserId,
                ModiDateTime = DateTime.Now,
                ModiUserId = input.InUserId
            };
            _historyRepository.Insert(historyDto);
           
            output.UpdateStatus = "Message:Success";
            return output;
        }

        [UnitOfWork]
        public UpdateStockOutput UpdateStockList(List<UpdateStockInput> input)
        {
           
            UpdateStockOutput output = new UpdateStockOutput();
            foreach (var item in input)
            {
                var query = from s in _stockRepository.GetAll()
                            where s.ProdCode == item.ProdCode
                            select s;
                var dto = query.FirstOrDefault();

                if (dto != null && dto.ProdCode == item.ProdCode)
                {
                    dto.Count += input.Count;
                    dto.CreationTime = DateTime.Now;
                    output.Id = dto.Id;
                    output.ProdCode = dto.ProdCode;
                    output.PreCount = dto.Count;
                }
                else
                {
                    dto = new Stock() { ProdCode = item.ProdCode, CreationTime = DateTime.Now, Count = item.Count };

                    output.ProdCode = dto.ProdCode;
                    output.PreCount = 0;
                }
                //_stockRepository.InsertOrUpdateAsync(dto);

                if (dto.Count < 0)
                {
                    throw new Exception("Message: Stock not enough");
                 
                }
                _stockRepository.InsertOrUpdateAsync(dto);
                
                var historyDto = new History
                {
                    ProdCode = item.ProdCode,
                    Changes = item.Count,
                    InDateTime = DateTime.Now,
                    InUserId = item.InUserId,
                    ModiDateTime = DateTime.Now,
                    ModiUserId = item.InUserId
                };
                _historyRepository.Insert(historyDto);

                output.UpdateStatus = "Message:Success";
                

            }

            return output;
        }
    }
}
