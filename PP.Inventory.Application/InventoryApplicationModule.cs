﻿using System.Reflection;
using Abp.Modules;

namespace PP.Inventory
{
    [DependsOn(typeof(InventoryCoreModule))]
    public class InventoryApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
