﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP.Inventory.Dto
{
    public  class UpdateStockOutput : Entity<long>
    {
        public string ProdCode { get; set; }
        public string UpdateStatus { get; set; }

        public int PreCount { get; set; }
    }
}
