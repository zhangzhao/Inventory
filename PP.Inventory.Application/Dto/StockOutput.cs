﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using PP.Inventory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP.Inventory.Dto
{
    [AutoMapFrom(typeof(Stock))]
    public class StockOutput : Entity<long>
    {
        public string ProdCode { get; set; }
        public int Count { get; set; }
    }
}
