﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP.Inventory.Dto
{
    public class UpdateStockInput : Entity<long>
    {
        public string ProdCode { get; set; }
        public int Count { get; set; }
        public string InUserId { get; set; }
    }
}
